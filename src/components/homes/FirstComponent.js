import React from 'react';
import "../../css/components/homes/FirstComponent.css";

const FirstComponent = () => {
    return (
        <div className="FirstComponent">
            <p>React</p>
            <p>Une bibliothèque JavaScript pour créer des interfaces utilisateurs</p>
            <div>
                <button>Bien démarrer</button>
                <a href="http://"><span>Suivre le tutoriel {">"} </span></a>
            </div>
        </div>
    );
}

export default FirstComponent;
