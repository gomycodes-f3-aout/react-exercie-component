import "../../css/components/homes/Main.css"
import FirstComponent from "./FirstComponent"

export default function Main() {
    return (
        <div className="body">
            <FirstComponent/>
        </div>
    )
}
