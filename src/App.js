import logo from './logo.svg';
import './App.css';
import TopUtility from './utilities/TopUtility';
import TopNavbar from './headers/TopNavbar';
import Main from './components/homes/Main';

function App() {
  return (
    <div>
      <div style={{position:'fixed', width: "100%", top: 0, zIndex: 3}}>
        <TopUtility/>
        <TopNavbar/>
      </div>
      <div>
        <Main/>
      </div>
    </div>
  );
}

export default App;
