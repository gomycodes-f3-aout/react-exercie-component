import "../css/headers/TopNavbar.css";
import logo from "../logo.svg";

const TopNavbar = () => {
    return (
        <div className="header-main">
            <div>
                <img src={logo} alt="" srcset="" />
                <span>React</span>
            </div>
            <div>
                <ul>
                    <li> <a href="http://"> <span>Docs</span></a></li>
                    <li> <a href="http://"> <span>Tutoriel</span></a></li>
                    <li> <a href="http://"> <span>Blog</span></a></li>
                    <li> <a href="http://"> <span>Communauté</span></a></li>
                </ul>
            </div>
            <div>
                <ul>
                    <li><input type="text" /></li>
                    <li> <a href="http://"> <span>v17.0.2</span></a></li>
                    <li> <a href="http://"> <span>Langues</span></a></li>
                    <li> <a href="http://"> <span>Github</span></a></li>
                </ul>
            </div>
        </div>
    );
}

export default TopNavbar;
